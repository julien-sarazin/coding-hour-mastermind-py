# Maître de la divination
Il est connu que mages et guerriers ne sont pas toujours d'accord sur la meilleure façon de régler un problème.
Et cela se confirme d'autant plus lorsqu'il s'agit de deviner ce à quoi l'autre pense.  

Chacun est certain de savoir quelle est la meilleure stratégie...

Mages Python, Guerriers Javascript, à vous de prouver lesquels d'entre-vous seront les plus à même de concevoir la meilleure strategie.



## 1- Règles Techniques
Votre challenge est de concevoir un système de jeu similaire au très connu "Mastermind".

Vous serez testés sur différents critères tel que : 

- Le respect des interfaces qui vous seront fournis
    - implique vous devrez développer dans les fonctions/méthodes qui vous auront été fournies,
    - implique d'offrir une utilisation telle que spécifiée lors des vérifications techniques
- Le respect des structures de données qui vous seront fournis
    - implique que vous devrez utiliser la/les structure(s) de donnée(s) initialement fournie(s).    
        
Les vérifications techniques sont les suivantes :

- L'entité retournée doit être une `classe`,
- Les fonctions/méthodes suivantes doivent être implémentée : 
    
    ```python
            '''{bool} Détermine si la partie est terminée.'''
            def done(self):
            ''' {bool} Détermine si la partie est gagnée. '''
            def successful(self):
            ''' {Array} Donne des indices les précédentes 
                tentatives du joueur. (cf 2.1) '''
            def results(self):
            ''' {Number} Donne le nombre de tentatives restantes '''
            def remaining_attempts(self):
            ''' {Array} Retourne la liste des précédentes tentatives '''
            def attempts(self):
            ''' {self} Permet à un joueur de tester une combinaison
                @param suit {Array} la combinaison '''
            def guess(self, suit):
    ```


## 2 - Règles fonctionnelles

1. Lorsqu'une partie démarre, **deux** informations sont nécessaires: La **séquence d'origine**, *sous la forme d'un tableau d'entiers*, et le **nombre de maximum de tentatives**, *sous la forme d'un entier positif*, pour trouver celle-ci.
2. Une partie qui démarre doit : 
    1. Ne pas être terminée,
    2. Ne doit pas être gagnée ou perdue,
    3. Avoir un nombre de tentatives restante égal à celui donné lors de la création de la partie,
    4. Doit permettre à un joueur de deviner la combinaison.
3. Lorsqu'un joueur tente une combinaison :
    1. Il doit pouvoir enchainer les tentatives sans attendre le retour du systeme
        
        ```python
        game = Mastermind([1, 2, 3], 10)
        game.guess([0, 0, 0]).guess([1, 0, 1]).guess([3, 1, 1])
        .....
        ```
    2. Cela doit impacter le nombre de tentative restantes 
    3. Chaque tentative doit générer un tableau d'indices permettant 
    d'aider l'utilisateur à trouver le bonne combinaison. (cf 3.4.2)
    4. Les indices doivent suivre les régles suivantes: 
        1. Les mauvaise valeurs seront remplacées par `-1`
        2. Les bonnes valeurs mais mal positionnées seront remplacées par `1`
        3. Et enfin, les bonnes valeurs correctement positionnées seront remplacées par `0`
4. Lors qu'un joueur atteint le nombre maximum de tentatives
    1. Les tentatives suivantes doivent être ignorées
    2. La partie doit être terminée
    3. Le status de la partie doit être mis à jour

5. Lors qu'un joueur trouve la bonne combinaison
    1. Les tentatives suivantes doivent être ignorées
    2. La partie doit être terminée
    3. Le status de la partie doit être mis à jour


**Mages**, **Guerriers** à vos claviers!

