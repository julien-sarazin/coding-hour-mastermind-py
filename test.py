#!/usr/bin/python3.5

import unittest
from sources.Mastermind import Mastermind as Sut


class Test(unittest.TestCase):
    def setUp(self):
        pass

    def test_mastermind_is_a_Class(self):
        sut = Sut([1, 3, 2], 10)

    def test_respect_interfaces(self):
        sut = Sut([1, 3, 2], 10)
        self.assertIsInstance(sut.done(), bool)
        self.assertIsInstance(sut.successful(), bool)
        self.assertIsInstance(sut.results(), list)
        self.assertIsInstance(sut.remaining_attempts(), int)
        self.assertIsInstance(sut.attempts(), list)
        self.assertIsNotNone(sut.guess([1, 2, 3]))

    def test_when_a_game_start_it_should_have_max_attempts_left(self):
        sequence = [1, 9, 3]
        max_attempts = 10

        sut = Sut(sequence, max_attempts)
        self.assertEqual(sut.remaining_attempts(), max_attempts)

    def test_when_a_game_start_it_should_not_be_done(self):
        sequence = [1, 9, 3]
        max_attempts = 10

        sut = Sut(sequence, max_attempts)
        self.assertEqual(sut.done(), False)

    def test_when_a_game_start_it_should_not_be_successful(self):
        sequence = [1, 9, 3]
        max_attempts = 10

        sut = Sut(sequence, max_attempts)
        self.assertEqual(sut.successful(), False)

    def test_when_a_game_start_it_should_allow_players_to_try_sequence(self):
        sequence = [1, 9, 3]
        max_attempts = 10

        sut = Sut(sequence, max_attempts)
        self.assertIsNotNone(sut.guess([1, 2, 3]))

    def test_when_a_player_tries_sequence_it_should_be_chainable(self):
        sequence = [1, 9, 3]
        max_attempts = 10

        sut = Sut(sequence, max_attempts)
        sut\
            .guess([3, 4, 5, 2, 4])\
            .guess([3, 4, 5, 2, 4])\
            .guess([3, 4, 5, 2, 4])\
            .guess([3, 4, 5, 2, 4])

    def test_when_a_player_tries_sequence_it_should_reduce_attempts_after_each_try(self):
        sequence = [1, 9, 3]
        max_attempts = 6

        sut = Sut(sequence, max_attempts)
        sut.guess([3, 4, 5, 2, 4])

        self.assertEqual(sut.remaining_attempts(), 5)

    def test_when_a_player_tries_sequence_it_should_keep_references_of_each_guess(self):
        sequence = [1, 9, 3]
        max_attempts = 6

        sut = Sut(sequence, max_attempts)
        sut.guess([3, 4, 5, 2, 4])
        sut.guess([3, 4, 5, 2, 4])
        sut.guess([3, 4, 5, 2, 4])

        self.assertEqual(len(sut.results()), 3)

    def test_when_a_player_tries_sequence_it_should_replace_guesses_by_the_proper_values(self):
        sequence = [1, 9, 3]
        max_attempts = 6

        sut = Sut(sequence, max_attempts)
        sut.guess([1, 3, 6])

        result = sut.results()[-1]
        self.assertEqual(result, [0, 1, -1])

    def test_when_a_player_reaches_the_maximum_attempts_limit_it_should_ignore_the_next_attempts(self):
        sequence = [1, 9, 3]
        max_attempts = 1

        sut = Sut(sequence, max_attempts)
        sut.guess([1, 3, 6]).guess([1, 3, 6])

        self.assertEqual(len(sut.results()), 1)

    def test_when_a_player_reaches_the_maximum_attempts_limit_it_should_update_successful_state(self):
        sequence = [1, 9, 3]
        max_attempts = 1

        sut = Sut(sequence, max_attempts)
        sut.guess([1, 3, 6]).guess([1, 3, 6])

        self.assertEqual(sut.successful(), False)

    def test_when_a_player_reaches_the_maximum_attempts_limit_it_should_update_done_status(self):
        sequence = [1, 9, 3]
        max_attempts = 1

        sut = Sut(sequence, max_attempts)
        sut.guess([1, 3, 6]).guess([1, 3, 6])

        self.assertEqual(sut.done(), True)

    def test_when_a_player_find_the_hidden_sequence_it_should_ignore_the_next_attempts(self):
        sequence = [1, 9, 3]
        max_attempts = 1

        sut = Sut(sequence, max_attempts)
        sut.guess([1, 9, 3]).guess([1, 3, 6])

        self.assertEqual(len(sut.results()), 1)

    def test_when_a_player_find_the_hidden_sequence_it_should_update_successful_state(self):
        sequence = [1, 9, 3]
        max_attempts = 1

        sut = Sut(sequence, max_attempts)
        sut.guess([1, 9, 3]).guess([1, 3, 6])

        self.assertEqual(sut.successful(), True)

    def test_when_a_player_find_the_hidden_sequence_limit_it_should_update_done_status(self):
        sequence = [1, 9, 3]
        max_attempts = 1

        sut = Sut(sequence, max_attempts)
        sut.guess([1, 9, 3]).guess([1, 3, 6])

        self.assertEqual(sut.done(), True)


if __name__ == '__main__':
    unittest.main()


